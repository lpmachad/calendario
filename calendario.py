#!/usr/bin/python3

# Diccionario para almacenar las actividades organizadas por fecha y hora
calendar = {}

# Función para agregar una actividad al calendario
def add_activity(date: str, time: str, activity: str):
    if date not in calendar:
        calendar[date] = {}
    calendar[date][time] = activity

# Función para obtener todas las actividades para una hora específica
def get_time(atime):
    activities = []
    for date, act in calendar.items():
        for time, activity in act.items():
            if time == atime:
                activities.append((date, time, activity))
    return activities

# Función para obtener todas las actividades en el calendario
def get_all():
    all_activities = []
    for date, act in calendar.items():
        for time, activity in act.items():
            all_activities.append((date, time, activity))
    return all_activities

# Función para obtener la fecha más ocupada y el número de actividades en ese día
def get_busiest():
    busiest = None
    busiest_no = 0
    for date, act in calendar.items():
        if len(act) > busiest_no:
            busiest = date
            busiest_no = len(act)
    return busiest, busiest_no

# Función para mostrar las actividades
def show(activities):
    for (date, time, activity) in activities:
        print(f"{date}. {time}: {activity}")

# Función para verificar si una fecha tiene el formato correcto (aaaa-mm-dd)
def check_date(date):
    fecha = date.split('-')

    if len(fecha) != 3:
        return False

    anyo, mes, dia = fecha

    if len(anyo) != 4 or not anyo.isdigit():
        return False
    if not (0000 <= int(anyo) <= 2100):
        return False
    if not (0 < int(mes) <= 12):
        return False
    if not (0 < int(dia) <= 31):
        return False
    return True

# Función para verificar si una hora tiene el formato correcto (hh:mm)
def check_time(time):
    try:
        hour, minute = map(int, time.split(':'))
        if 0 <= hour <= 24 and 0 <= minute <= 60:
            return True
        else:
            return False
    except ValueError:
        return False

# Función para obtener la actividad del usuario
def get_activity():
    while True:
        date = input('Fecha: ')
        if check_date(date):
            while True:
                time = input('Hora: ')
                if check_time(time):
                    activity = input('Actividad: ')
                    return date, time, activity
                else:
                    print('El formato hora(hh:mm) es incorrecto. Intentalo de nuevo.')
        else:
            print('El formato fecha(aaaa-mm-dd) es incorrecto. Intentalo de nuevo.')

# Función para mostrar el menú y obtener la opción del usuario
def menu():
    print("A. Introduce actividad")
    print("B. Lista todas las actividades")
    print("C. Día más ocupado")
    print("D. Lista de las actividades de cierta hora dada")
    print("X. Terminar")
    asking = True
    while asking:
        option = input("Opción: ").upper()
        if option in ['A', 'B', 'C', 'D', 'X']:
            asking = False
    return option

# Función para ejecutar la opción seleccionada por el usuario
def run_option(option):
    if option == 'A':
        date, time, activity = get_activity()
        add_activity(date, time, activity)
    elif option == 'B':
        activities = get_all()
        show(activities)
    elif option == 'C':
        busiest, busiest_no = get_busiest()
        print(f"El día más ocupado es el {busiest}, con {busiest_no} actividad(es).")
    elif option == 'D':
        atime = input("Hora: ")
        activities = get_time(atime)
        show(activities)

# Función principal del script
def main():
    proceed = True
    while proceed:
        option = menu()
        if option == 'X':
            proceed = False
        else:
            run_option(option)

# Ejecuta el script si es el programa principal
if __name__ == "__main__":
    main()

